import { Map } from 'immutable';
import {
  UPDATE_USER,
  READ_USER
} from 'actions/user';

const initialState = Map({
  name: null,
  email: null
});

const actionsMap = {

  [UPDATE_USER]: (state, data) => {
    return state.merge(Map(data));
  },

  [READ_USER]: (state, data) => {
    return state.merge(Map(data));
  }

};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
