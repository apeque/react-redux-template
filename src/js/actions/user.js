import api from 'api';

export const READ_USER   = 'READ_USER';
export const UPDATE_USER = 'UPDATE_USER';

export function readUser(){
  return dispatch => {
    api.readUser().then(data => {
      dispatch({
        type: READ_USER,
        ...data
      });
    });
  }
};

export function updateUser(data){
  return {
    type: UPDATE_USER,
    ...data
  };
};
