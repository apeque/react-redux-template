import React, { Component } from 'react';
import Routes from 'config/routes';

export default class App extends Component {

  constructor(props){
    super(props);
  };

  render() {
    return (
      <div className='App'>
        <Routes />
      </div>
    );
  };

};
