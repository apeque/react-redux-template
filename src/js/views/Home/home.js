import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { readUser, updateUser } from 'actions/user';

@connect(state => ({
  name:  state.user.get('name'),
  email: state.user.get('email')
}))
export class Home extends Component {

  componentDidMount(){
    this.props.dispatch(readUser());
  };

  ready(){
    return (this.props.email)
  };

  update(){
    this.props.dispatch(updateUser({
      name: 'Another Name'
    }));
  };

  render(){
    if(this.ready()){
      return (
        <div>
          <ul>
            <li>Name: {this.props.name}</li>
            <li>Email: {this.props.email}</li>
          </ul>
          <button onClick={this.update.bind(this)}>Update</button>
        </div>
      );
    }
    else {
      return (
        <div>Loading...</div>
      );
    }
  }

};
