import promisePolyfill from 'es6-promise';
import 'isomorphic-fetch';

promisePolyfill.polyfill();

async function readUser() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({
        name: 'Erik Smith',
        email: 'erik@apeque.com'
      })
    },
    500);
  });
};

export default {
  readUser,
};
