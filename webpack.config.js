const webpack = require('webpack');
const path = require('path');
const paths = require('./webpack/config').paths;
const outputFiles = require('./webpack/config').outputFiles;
const rules = require('./webpack/config').rules;
const plugins = require('./webpack/config').plugins;
const resolve = require('./webpack/config').resolve;
const devServer = require('./webpack/dev-server').devServer;

const IS_PRODUCTION = require('./webpack/config').IS_PRODUCTION;
const IS_DEVELOPMENT = require('./webpack/config').IS_DEVELOPMENT;

const DashboardPlugin = require('webpack-dashboard/plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const entry = [
  path.join(paths.javascript, 'client.js'),
];

plugins.push(
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    filename: outputFiles.vendor,
    minChunks(module) {
      const context = module.context;
      return context && context.indexOf('node_modules') >= 0;
    },
  }),
  new HtmlWebpackPlugin({
    template: path.join(paths.source, 'index.html'),
    path: paths.build,
    filename: 'index.html',
    minify: {
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true,
      removeComments: true,
      useShortDoctype: true,
    },
  })
);

if (IS_DEVELOPMENT) {
  plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new DashboardPlugin()
  );
  rules[0].use.unshift('react-hot-loader/webpack');
  entry.unshift('react-hot-loader/patch');
}

module.exports = {
  devtool: IS_PRODUCTION ? false : 'cheap-eval-source-map',
  context: paths.javascript,
  watch: !IS_PRODUCTION,
  entry,
  output: {
    path: paths.build,
    publicPath: '/',
    filename: outputFiles.client,
  },
  module: {
    rules,
  },
  resolve,
  plugins,
  devServer,
};
